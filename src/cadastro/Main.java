package cadastro;

import java.util.Scanner;

public class Main {
	private static Scanner entrada;

	public static void menu() {
		System.out.println("\n\n### Sistema para Cadastro de Clientes ###");  
		System.out.println("\n ====================================="); 
		System.out.println("  |     1 - Cadastro de Cliente         |");
		System.out.println("  |     2 - Sair do Sistema             |");
		System.out.println("    ===================================\n");  
		System.out.println("       Informe a op��o desejada           ");
	}

	public static void cadastrarl() {
		ClienteDAO v = new ClienteDAO();

		System.out.println("Preencha os dados corretamente");
		System.out.println("Campos marcados com * s�o obrigat�rios");
		v.cadastraNome();
		v.cadastraSexo();
		v.cadastraEstadoCivil();
		v.cadastraDataNascimento();
		v.cadastraRG();
		v.cadastraProfissao();
		v.cadastraFaixaSalarial();
		v.cadastraTimeDeFutebol();
		v.cadastraLogradouro();
		v.cadastraNumero();
		v.cadastraComplemento();
		v.cadastraBairro();
		v.cadastraCEP();
		v.cadastraCidade();
		v.cadastraUF();
		v.cadastraReferencias();
		v.cadastraTelefoneResidencial();
		v.cadastraTelefoneCelular();
		v.cadastraEmail();
		v.validaTipoPessoa("Informe o tipo de pessoa (Fisica ou Juridica)");
		v.cadastraMontante();
		v.cadastraInfoAdicionais();

		System.out.println("Cadastro preenchido");
		System.out.println("ID: " + v.c.getId());
		System.out.println("Nome Completo: " + v.c.getNomeCompleto());
		System.out.println("Sexo: " + v.c.getSexo());
		System.out.println("Estado Civil: " + v.c.getEstadoCivil());
		System.out.println("Data de Nascimento: " + v.c.getDataNasc());
		System.out.println("RG: " + v.c.getRg());
		System.out.println("Profiss�o: " + v.c.getProfissao());
		System.out.println("Faixa Salarial*: " + v.c.getFaixaSalarial());
		System.out.println("Time de Futebol: " + v.c.getTimeDeFutebol());
		System.out.println("Logradouro*: " + v.c.getLogradouro());
		System.out.println("N�mero*: " + v.c.getNumeroEndereco());
		System.out.println("Complemento: " + v.c.getComplemento());
		System.out.println("Bairro: " + v.c.getBairro());
		System.out.println("CEP: " + v.c.getCep());
		System.out.println("Cidade: " + v.c.getCidade());
		System.out.println("UF: " + v.c.getUf());
		System.out.println("Refer�ncias: " + v.c.getReferencias());
		System.out.println("Telefone Residencial: " + v.c.getTelefoneResidencial());
		System.out.println("Telefone Celular: " + v.c.getTelefoneCelular());
		System.out.println("E-mail: " + v.c.getEmail());
		System.out.println("Tipo Pessoa*: " + v.c.getTipoPessoa());
		System.out.println("CPF:" + v.c.getCpf());
		System.out.println("CNPJ: " + v.c.getCnpj());
		System.out.println("Data Cadastro: " + v.c.getDataCadastro());
		System.out.println("Montante: " + v.c.getMontante());
		System.out.println("Informa��es Adicionais: " + v.c.getInfoAdicionais());
	}

	// main do sistema
	public static void main(String[] args) {
		int opcao;
		entrada = new Scanner(System.in);
		
		do {
			menu();
			opcao = entrada.nextInt();
			
			switch(opcao){
	            case 1:
	                cadastrarl();
	                break;
	                
	            case 2:
	               System.out.println("      SISTEMA ENCERRADO.");
	               //System.exit(0);
	                break;
	                
	            default:
	                System.out.println("      OP��O INV�LIDA.");
			}
		} while(opcao != 2);
	}
}
