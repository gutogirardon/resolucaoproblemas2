package cadastro;

/*
 * Classe com os m�todos de valida��o
 * O padr�o DAO � um padr�o de projeto que abstrai e encapsula os mecanismos
 * de acesso a dados escondendo os detalhes da execu��o da origem dos dados.
 * 
 */

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.regex.Matcher; // classes Matcher do pacote regex - expressoes regulares
import java.util.regex.Pattern;

import valida.ValidaCNPJ;
import valida.ValidaCPF;

public class ClienteDAO {

	Scanner s = new Scanner(System.in);
	public Cliente c = new Cliente();

	/*
	 * 
	 * M�todo para aceitar apenas letras
	 */

	public boolean validaLetras(String s) {
		Pattern pattern = Pattern.compile("[^A-Za-z ]");
		Matcher matcher = pattern.matcher(s);
		if (matcher.find()) {
			System.out.println(" Caracteres Invalidos \n Apenas Letras ");
			return true;
		} else {
			return false;
		}
	}

	/*
	 * M�todo para n�o aceitar campo em branco
	 */

	public boolean validaCampoNulo(String s) {
		if (s.isEmpty()) {
			System.out.println(" Campo Obrigatorio ");
			return false;
		} else {
			return true;
		}
	}

	/*
	 * M�todo para aceitar apenas n�meros
	 */

	public boolean validaNumeros(String s) {
		Pattern pattern = Pattern.compile("[^0-9]");
		Matcher matcher = pattern.matcher(s);
		if (matcher.find()) {
			System.out.println(" Caracteres Invalidos \n Apenas Numeros ");
			return false;
		} else {
			return true;
		}
	}

	/*
	 * M�todo para aceitar caracteres especiais
	 */

	public boolean validaNumerosCaracEspeciais(String s) {
		Pattern pattern = Pattern.compile("[A-Za-z]");
		Matcher matcher = pattern.matcher(s);
		if (matcher.find()) {
			System.out.println(" Caracteres Invalidos \n Apenas Numeros ou Carcteres Especiais ");
			return false;
		} else {
			return true;
		}
	}

	/*
	 * M�todo para aceitar apenas caracteres alfanumericos
	 */

	public boolean validaAlfanumericos(String s) {
		Pattern pattern = Pattern.compile("[^A-Za-z 0-9]");
		Matcher matcher = pattern.matcher(s);
		if (matcher.find()) {
			System.out.println(" Caracteres Invalidos \n Apenas Caracteres Alfanumericos ");
			return false;
		} else {
			return true;
		}
	}
	
	/*
	 * Metood que aceita 300 caracteres em campo
	 */
	public boolean validaMax300Carac(String s) {
		if (s.length() > 300) {
			System.out.println("Maximo 300 caracteres");
			return false;
		} else {
			return true;
		}
	}
	
	/*
	 * M�todo que valida formato telefone
	 */
	
	public boolean validaFormatoTelefone(String s) {
		if (s.matches("\\d{2} \\d{8,9}")) {
			return true;
		} else {
			return false;
		}
	}
	
	/*
	 * M�todo que aceita apenas alfanumericos especiais
	 */
	public boolean validaAlfanumericosEspeciais(String s) {
		Pattern pattern = Pattern.compile("[^A-Za-z 0-9$.,]");
		Matcher matcher = pattern.matcher(s);
		if (matcher.find()) {
			System.out.println(" Caracteres Invalidos \n Apenas Caracteres Alfanumericos ");
			return false;
		} else {
			return true;
		}
	}

	/*
	 * M�todo que aceita at� 20 caracteres em um campo
	 */
	public boolean validaMax20Carac(String s) {
		if (s.length() > 20) {
			System.out.println("Maximo 20 caracteres");
			return false;
		} else {
			return true;
		}
	}
	/*
	 * M�todo que aceita at� 50 caracteres em um campo
	 */

	public boolean validaMax50Carac(String s) {
		if (s.length() > 50) {
			System.out.println("Maximo 50 caracteres");
			return false;
		} else {
			return true;
		}
	}
	
	public boolean validaMax12Carac(String s) {
		if (s.length() > 12) {
			System.out.println("Maximo 12 caracteres");
			return false;
		} else {
			return true;
		}
	}

	/*
	 * M�todo para cadastrar nome
	 */

	public void cadastraNome() { // cadastro nome
		System.out.println("Nome*: ");
		String s;
		s = this.s.nextLine();
		validaNomeCompleto(s);
	}

	/*
	 * M�todo para validar nome
	 */

	public void validaNomeCompleto(String s) {
		if (validaCampoNulo(s) == false) {
			cadastraNome();
		} else if (validaLetras(s) == true) {
			cadastraNome();
		} else {
			c.setNomeCompleto(s);
		}
	}

	/*
	 * M�todo para cadastrar sexo
	 */
	public void cadastraSexo() {
		System.out.println("Sexo*: ");
		String s;
		s = this.s.nextLine();
		validaSexo(s);
	}

	/*
	 * M�todo para validar sexo
	 */

	public void validaSexo(String s) {
		if (validaCampoNulo(s) == false) {
			cadastraSexo();
		} else if (validaMasculinoFeminino(s) == false) {
			cadastraSexo();
		} else {
			c.setSexo(s);
		}
	}

	/*
	 * M�todo para validar se � igual masculino ou feminino
	 */

	public boolean validaMasculinoFeminino(String s) {
		String m = "masculino";
		String f = "feminino";
		if (s.equals(m) || s.equals(f)) {
			return true;
		} else {
			System.out.println(" Caracteres Invalidos | Digite masculino ou feminino ");
			return false;
		}
	}

	/*
	 * M�todo para cadastrar estado civil
	 */

	public void cadastraEstadoCivil() {
		System.out.println("Estado Civil: ");
		String s;
		s = this.s.nextLine();
		validaEstadoCivil(s);
	}

	/*
	 * M�todo para validar estado civil
	 */

	public void validaEstadoCivil(String s) {
		if (validaCampoNulo(s) == false) {
			cadastraEstadoCivil();
		} else if (validaLetras(s) == true) {
			cadastraEstadoCivil();
		} else {
			c.setEstadoCivil(s);
		}
	}

	/*
	 * Metodo que cadastra o =
	 */
	public void cadastraRG() {
		System.out.println("RG*: ");
		String s;
		s = this.s.nextLine();
		validaRG(s);
	}

	/*
	 * M�todo que valida RG
	 */

	public void validaRG(String s) {
		if (validaCampoNulo(s) == false) {
			cadastraRG();
		} else if (validaNumerosCaracEspeciais(s) == false) {
			cadastraRG();
		} else if (validaMax12Carac(s) == false) {
			cadastraRG();
		} else {
			c.setRg(s);
		}
	}

	/*
	 * Metodo que cadastra profiss�o
	 */
	public void cadastraProfissao() {
		System.out.println("Profissao*: ");
		String s;
		s = this.s.nextLine();
		validaProfissao(s);
	}

	/*
	 * M�todo que valida profiss�o
	 */

	public void validaProfissao(String s) {
		if (validaCampoNulo(s) == false) {
			cadastraProfissao();
		} else if (validaLetras(s) == true) {
			cadastraProfissao();
		} else if (validaMax50Carac(s) == false) {
			cadastraProfissao();
		} else {
			c.setProfissao(s);
		}
	}

	/*
	 * M�todo que cadastra faixa salarial
	 */
	public void cadastraFaixaSalarial() {
		System.out.println("Faixa Salarial*: ");
		String s;
		s = this.s.nextLine();
		validaFaixaSalarial(s);
	}

	/*
	 * Metodo que valida o campo Faixa Salarial
	 */
	public void validaFaixaSalarial(String s) {
		if (validaCampoNulo(s) == false) {
			cadastraFaixaSalarial();
		} else if (validaAlfanumericos(s) == false) {
			cadastraFaixaSalarial();
		} else {
			c.setFaixaSalarial(s);
		}

	}

	/*
	 * m�todo que cadastra time de futebol
	 */
	public void cadastraTimeDeFutebol() {
		System.out.println("Time de Futebol: ");
		String s;
		s = this.s.nextLine();
		validaTimeFutebol(s);
	}

	/*
	 * metodo que valida o time de futebol
	 */
	public void validaTimeFutebol(String s) {
		if (validaLetras(s) == true) {
			cadastraTimeDeFutebol();
		} else if (validaMax50Carac(s) == false) {
			cadastraTimeDeFutebol();
		} else {
			c.setTimeDeFutebol(s);
		}
	}

	/*
	 * Metodo que cadastra o logradouro
	 */
	public void cadastraLogradouro() {
		System.out.println("Logradouro*: ");
		String s;
		s = this.s.nextLine();
		validaLogradouro(s);
	}

	/*
	 * m�todo que valida o logradouro
	 */
	public void validaLogradouro(String s) {
		if (validaCampoNulo(s) == false) {
			cadastraLogradouro();
		} else if (validaAlfanumericos(s) == false) {
			cadastraLogradouro();
		} else if (validaMax50Carac(s) == false) {
			cadastraLogradouro();
		} else {
			c.setLogradouro(s);
		}
	}

	/*
	 * m�todo que cadastra o n�mero
	 */
	public void cadastraNumero() {
		System.out.println("Numero*: ");
		String s;
		s = this.s.nextLine();
		validaNumero(s);
	}

	/*
	 * Metodo que valida o campo Numero
	 */
	public void validaNumero(String s) {
		if (validaCampoNulo(s) == false) {
			cadastraNumero();
		} else if (validaNumeros(s) == false) {
			cadastraNumero();
		} else {
			c.setNumeroEndereco(s);
		}
	}

	/*
	 * M�todo que cadastra Complemento
	 */

	public void cadastraComplemento() {
		System.out.println("Complemento: ");
		String s;
		s = this.s.nextLine();
		validaComplemento(s);
	}

	/*
	 * Metodo que valida o campo Complemento
	 */
	public void validaComplemento(String s) {
		if (validaAlfanumericos(s) == false) {
			cadastraComplemento();
		} else if (validaMax20Carac(s) == false) {
			cadastraComplemento();
		} else {
			c.setComplemento(s);
		}
	}

	/*
	 * M�todo que cadastra o campo Bairro
	 */
	public void cadastraBairro() {
		System.out.println("Bairro: ");
		String s;
		s = this.s.nextLine();
		validaBairro(s);
	}

	/*
	 * Metodo que valida o campo Bairro
	 */
	public void validaBairro(String s) {
		if (validaAlfanumericos(s) == false) {
			cadastraBairro();
		} else if (validaMax50Carac(s) == false) {
			cadastraBairro();
		} else {
			c.setBairro(s);
		}
	}

	/*
	 * Metodo que cadastra CEP
	 */
	public void cadastraCEP() {
		System.out.println("CEP: ");
		String s;
		s = this.s.nextLine();
		validaCEP(s);
	}

	/*
	 * M�todo que valida CEP
	 */
	public void validaCEP(String s) {
		if (validaNumerosCaracEspeciais(s) == false) {
			cadastraCEP();
		} else {
			c.setCep(s);
		}
	}

	/*
	 * M�todo que cadastra cidade
	 */
	public void cadastraCidade() {
		System.out.println("Cidade*: ");
		String s;
		s = this.s.nextLine();
		validaCidade(s);
	}

	/*
	 * Metodo que valida a Cidade
	 */
	public void validaCidade(String s) {
		if (validaCampoNulo(s) == false) {
			cadastraCidade();
		} else if (validaLetras(s) == true) {
			cadastraCidade();
		} else if (validaMax50Carac(s) == false) {
			cadastraCidade();
		} else {
			c.setCidade(s);
		}
	}

	/*
	 * Metodo que cadastra UF
	 */
	public void cadastraUF() {
		System.out.println("UF*: ");
		String s;
		s = this.s.nextLine();
		validaUF(s);
	}

	/*
	 * M�todo que valida UF
	 */
	public void validaUF(String s) {
		if (validaCampoNulo(s) == false) {
			cadastraUF();
		} else if (validaLetras(s) == true) {
			cadastraUF();
		} else {
			c.setUf(s);
		}
	}

	/*
	 * M�todo que cadastra Referencias
	 */
	public void cadastraReferencias() {
		System.out.println("Referencias: ");
		String s;
		s = this.s.nextLine();
		validaReferencias(s);
	}

	/*
	 * Metodo que valida Referencias
	 */
	public void validaReferencias(String s) {
		if (validaAlfanumericos(s) == false) {
			cadastraReferencias();
		} else if (validaMax50Carac(s) == false) {
			cadastraReferencias();
		} else {
			c.setReferencias(s);
		}
	}

	/*
	 * Metodo que cadastra o telefone residencial
	 */
	public void cadastraTelefoneResidencial() {
		System.out.println("Telefone Residencial: Use o padr�o 11 12341234");
		String s;
		s = this.s.nextLine();
		validaTelefoneResidencial(s);
	}

	/*
	 * M�todo que valida o telefone residencial
	 */
	public void validaTelefoneResidencial(String s) {
		if (validaFormatoTelefone(s) == false) {
			cadastraTelefoneResidencial();
		} else {
			c.setTelefoneResidencial(s);
		}
	}

	/*
	 * M�todo que cadastra o telefone celular
	 */

	public void cadastraTelefoneCelular() {
		System.out.println("Telefone Celular: Use o padr�o 11 12341234");
		String s;
		s = this.s.nextLine();
		validaTelefoneCelular(s);
	}
	
	/*
	 * M�todo que valida o telefone celular
	 */
	
	public void validaTelefoneCelular(String s) {
		if (validaFormatoTelefone(s) == false) {
			cadastraTelefoneCelular();
		} else {
			c.setTelefoneCelular(s);
		}
	}

	/*
	 * M�todo que cadastra o campo email
	 */
	public void cadastraEmail() {
		System.out.println("E-mail: Use o padr�o usuario@email.com");
		String s;
		s = this.s.nextLine();
		validaEmail(s);
	}
	
	/*
	 * Metodo que valida o campo E-mail
	 */
	public void validaEmail(String s) {
		if (validaFormatoEmail(s) == false) {
			cadastraEmail();
		} else {
			c.setEmail(s);
		}
	}
	
	/*
	 * Metodo que valida formato do email
	 */
	public boolean validaFormatoEmail(String s) {
		Pattern p = Pattern.compile(".+@.+\\.[a-z]+");
		Matcher m = p.matcher(s);
		if (m.find()) {
			return true;
		} else {
			return false;
		}
	}
	
	public String validaTipoPessoa(String message) {
		String s;
		do {
			System.out.print(message + "");
			s = this.s.nextLine();
			String fisica = "Fisica";
			String juridica = "Juridica";
			if (s.isEmpty()) {
				System.out.println(" Campo Obrigat�rio \n");
			} if (fisica.equals(s)) {
				c.setTipoPessoa(s);
				validaCPF("CPF*: \n");
				return s;
			} if (juridica.equals(s)) {
				c.setTipoPessoa(s);
				validaCNPJ("CNPJ*: \n");
				return s;
			} else {
				System.out.println("Caracteres Inv�lidos | Escolha Pessoa Fisica ou Juridica");
			}
		} while (true);
	}
	
	/*
	 * Metodo que valida o campo CPF
	 */
	public String validaCPF(String message) {
		String s;
		String CPF;
		do {
			System.out.print(message + "");
			s = this.s.nextLine();
			CPF = s;
			CPF = CPF.replace("-", "");
			CPF = CPF.replace(".", "");
			Pattern pattern = Pattern.compile("[A-Za-z]");
			Matcher matcher = pattern.matcher(s);
			if (s.isEmpty()) {
				System.out.println(" Campo Obrigat�rio \n");
			} else if (matcher.find()) {
				System.out.println(" Caracteres Inv�lidos \n Apenas N�meros ou Caracteres Especiais ");
			} else if (ValidaCPF.isCPF(CPF) == false) {
				System.out.printf("CPF Inv�lido \n Informe um CPF v�lido \n");
			} else {
				c.setCpf(s);
				return s;
			}
		} while (true);
	}

	/*
	 * Metodo que valida o campo CNPJ
	 */
	public String validaCNPJ(String message) {
		String s;
		String CNPJ;
		do {
			System.out.print(message + "");
			s = this.s.nextLine();
			CNPJ = s;
			CNPJ = CNPJ.replace("-", "");
			CNPJ = CNPJ.replace(".", "");
			CNPJ = CNPJ.replace("/", "");
			Pattern pattern = Pattern.compile("[A-Za-z]");
			Matcher matcher = pattern.matcher(s);
			if (s.isEmpty()) {
				System.out.println(" Campo Obrigat�rio \n");
			} else if (matcher.find()) {
				System.out.println(" Caracteres Inv�lidos \n Apenas N�meros ou Caracteres Especiais ");
			} else if (ValidaCNPJ.isCNPJ(CNPJ) == false) {
				System.out.printf("CNPJ Inv�lido \n Informe um CNPJ V�lido \n");
			} else {
				c.setCnpj(s);
				return s;
			}
		} while (true);
	}
	
	
	
	
	
	
	
	
	/*
	 * Metodo que cadastra montante
	 */
	public void cadastraMontante() {
		System.out.println("Montante: ");
		String s;
		s = this.s.nextLine();
		validaMontante(s);
	}
	
	
	/*
	 * Metodo que valida o campo Montante
	 */
	public void validaMontante(String s) {
		if (validaAlfanumericosEspeciais(s) == false) {
			cadastraMontante();
		} else {
			c.setMontante(s);
		}
	}

	/*
	 * Metodo que cadastra informa��es adicionais
	 */
	public void cadastraInfoAdicionais() {
		System.out.println("Informacoes Adicionais: ");
		String s;
		s = this.s.nextLine();
		validaInfoAdicionais(s);
	}
	
	/*
	 * M�todo que valida informa��es adicionais
	 */
	public void validaInfoAdicionais(String s) {
		if (validaAlfanumericos(s) == false) {
			cadastraInfoAdicionais();
		} else if (validaMax300Carac(s) == false) {
			cadastraInfoAdicionais();
		} else {
			c.setInfoAdicionais(s);
		}
	}

	/*
	 * Metodo que valida formato de Datas
	 */
	public void cadastraDataNascimento() {
		do {
			System.out.println("Data de Nascimento: ");
			String s;
			s = this.s.nextLine();
			validaDataNascimento(s);
		} while (false);
	}
	
	public void validaDataNascimento(String s) {
		if (validaFormatoData(s) == false) {
			cadastraDataNascimento();
		}
	}
	
	public boolean validaFormatoData(String s) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setLenient(false);
		Date data = null;
		try {
			data = sdf.parse(s);
			if (validaDataMaior(data) == true) {
				c.setDataNasc(data);
			}
			return true;
		} catch (Exception e) {
			// Se o formato for errado cai na excessao
			System.out.println(" Data invalida \n Utilize o padrao dd/MM/yyyy \n");
			return false;
		}
	}

	public boolean validaDataMaior(Date data) {
		if (data.after(new Date())) {
			System.out.println("Data invalida \n A data informada nao pode ser maior que a data atual");
			cadastraDataNascimento();
			return false;
		} else {
			return true;
		}
	}
}
